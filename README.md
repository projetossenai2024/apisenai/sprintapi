# Sprint API Users



> Esta API RESTful está sendo desenvolvida para estudo de JavaScript e implementa as funcionalidades básicas de CRUD (Criar, Ler, Atualizar, Deletar) para gerenciar usuários.

### Ajustes e melhorias

O projeto ainda está em desenvolvimento e as próximas atualizações serão voltadas nas seguintes tarefas:

- [x] Criar o app usando Express
- [x] Criar o CRUD de usuários
- [x] Criar as rotas HTTP
- [x] Adicionar verificações na função updateUser
- [x] Criar lógica para efetuar login usando JWT
- [ ] Criar função para buscar usuários pelo ID
- [ ] Refatorar código para melhor execução

## 💻 Pré-requisitos

* Node.js

**Passos:**

1. **Instalar o Node.js:** Acesse o site oficial <https://nodejs.org/en> e baixe a versão mais recente para o seu sistema operacional.


## 🚀 Instalando Sprint API

Para instalar o Sprint API, siga estas etapas:

1. **Clonar o projeto:** Utilize o comando abaixo para clonar o projeto para o seu computador.

```bash
git clone https://gitlab.com/projetossenai2024/apisenai/sprintapi.git
```

2. **Instalar as dependências:** Navegue até a pasta do projeto e execute o comando:

```bash
npm install
```

Com isto as dependências serão instaladas.

## ☕ Usando Sprint API

Para usar a Sprint API, siga estas etapas:

**Iniciar a API:**

1. Execute o comando `npm run start:dev` no terminal.
2. A API estará disponível na porta `5000`.

**Consumir a API no Postman:**

1. Abra o Postman e crie uma nova requisição.
2. Selecione o método HTTP desejado (POST, GET, PUT ou DELETE).
3. Insira a URL da API: `http://localhost:5000/`.
4. Para o método POST, inclua o corpo da requisição com os dados do usuário no formato JSON.
5. Clique em "Enviar" para executar a requisição.


Adicione comandos de execução e exemplos que você acha que os usuários acharão úteis. Fornece uma referência de opções para pontos de bônus!

## 📫 Contribuindo para Sprint API

Para contribuir com Sprint API, siga estas etapas:

1. Bifurque este repositório.
2. Crie um branch: `git checkout -b <nome_branch>`.
3. Faça suas alterações e confirme-as: `git commit -m '<mensagem_commit>'`
4. Envie para o branch original: `git push origin sprintApi / <local>`
5. Crie a solicitação de pull.

Como alternativa, consulte a documentação do GitHub em [como criar uma solicitação pull](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## 🤝 Colaboradores

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/igordmouraa">
        <img src="https://avatars.githubusercontent.com/u/127807075" width="100px;" alt="Foto do Igor Moura no GitHub"/><br>
        <sub>
          <b>Igor Moura</b>
        </sub>
      </a>
    </td>
  </tr>
</table>

## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE) para mais detalhes.