const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken")
let lastId = 0;

class UserController {
  static users = []; //Local onde são armazenados os usuários cadastrados

  //Cadastrar usuário
  static async createUser(req, res) {
    const { name, email, password } = req.body;
    const passwordHashed = bcrypt.hashSync(password, 10);

    try {
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      const isEmailValid = emailRegex.test(email);
      const isUserValid = name.length >= 4 && password.length >= 6;

      // Verificar se todos os campos são preenchidos corretamente
      if (!name || !email || !password) {
        return res.status(400).json({ message: 'Todos os campos devem ser preenchidos' });
      } else if (!isEmailValid) {
        return res.status(400).json({ message: 'Email inválido' });
      } else if (!isUserValid) {
        return res.status(400).json({ message: 'Nome ou senha inválido' });
      } else {
        // Verificar se o email já está em uso
        const emailExists = UserController.users.some(user => user.email === email);
        if (emailExists) {
          return res.status(400).json({ message: 'Este endereço de e-mail já foi cadastrado' });
        }

        const newUser = {
          name,
          email,
          passwordHashed,
          id: ++lastId,
        };

        UserController.users.push(newUser);
        return res.status(201).json({ message: 'Usuário cadastrado com sucesso' });
      }
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Erro ao cadastrar usuário." });
    }
  }

  static async getAllUsers(req, res) {
    res.status(200).json({ users: UserController.users });
  }

  static async updateUser(req, res) {
    const { id } = req.params;
    const { name, email, password } = req.body;
    const passwordHashed = bcrypt.hashSync(password, 10);

    try {
      // Verifica se o usuário existe
      const userToUpdate = UserController.users.find(user => user.id === parseInt(id));
      if (!userToUpdate) {
        return res.status(404).json({ message: 'Usuário não encontrado' });
      }

      // Verificações
      const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      const isEmailValid = !email || emailRegex.test(email);
      const isUserValid = name.length >= 4 && password.length >= 6;

      if (!name || !password) {
        return res.status(400).json({ message: 'Todos os campos devem ser preenchidos' })
      } else if (!isEmailValid) {
        return res.status(400).json({ message: 'Email inválido' })
      } else if (!isUserValid) {
        return res.status(400).json({ message: 'Nome ou senha inválido' })
      } else {
        // Verifica se o email já está em uso
        const emailExists = UserController.users.find(user => user.email === email);
        if (emailExists) {
          return res.status(400).json({ message: 'Este endereço de e-mail já foi cadastrado' })
        }
        // Atualiza o usuário encontrado na array
        userToUpdate.name = name;
        userToUpdate.passwordHashed = passwordHashed;

        // Atualiza o email somente se ele for fornecido na requisição
        if(!email){
          userToUpdate.email = email;
        }

        return res.status(200).json({ message: 'Usuário atualizado com sucesso' });
      }
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Erro ao atualizar usuário." });
    }
  }

  static async deleteUser(req, res) {
    const { id } = req.params;
    try {
      // Verifica se o usuário existe
      const userIndex = UserController.users.findIndex(user => user.id === parseInt(id));
      if (userIndex === -1) {
        return res.status(404).json({ message: 'Usuário não encontrado' });
      }

      // Remover o usuário da array
      UserController.users.splice(userIndex, 1);

      return res.status(200).json({ message: 'Usuário excluído com sucesso' });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: "Erro ao excluir usuário." });
    }
  }

  static async login(req, res) {
    const { email, password } = req.body;

    try {
        // Verificar se o email e a senha estão presentes
        if (!email || !password) {
            return res.status(400).json({ message: 'Email e senha devem ser fornecidos' });
        }

        // Procurar o usuário com o email fornecido
        const user = UserController.users.find(user => user.email === email);
        
        if (!user) {
            return res.status(404).json({ message: 'Usuário não encontrado' });
        }

        // Verificar se a senha fornecida corresponde à senha do usuário
        const passwordMatch = bcrypt.compareSync(password, user.passwordHashed);
        
        if (!passwordMatch) {
            return res.status(401).json({ message: 'Credenciais inválidas' });
        }

        // Se tudo estiver correto, gerar e retornar um token de autenticação (exemplo usando JWT)
        const token = jwt.sign({ id: user.id, email: user.email }, 'your_secret_key', { expiresIn: '1h' });

        return res.status(200).json({ token });

    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Erro ao fazer login." });
    }
}

}

module.exports = UserController;
