const router = require('express').Router();
const userController = require('../controller/userController');

////////////////////////////////////////////////////////////////////////

// Usuários
router.post('/createUser/', userController.createUser);
router.get('/getAllUsers/', userController.getAllUsers);
router.put('/updateUser/:id', userController.updateUser);
router.delete('/deleteUser/:id', userController.deleteUser);

// Login
router.post('/login', userController.login);

module.exports = router