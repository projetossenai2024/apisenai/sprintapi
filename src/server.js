const app = require("./index");
const Port = 5000;
app.listen(Port, () => {
    console.log(`Servidor conectado e rodando na porta ${Port} 💾`)
});
